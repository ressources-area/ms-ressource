FROM openjdk:11.0.7-slim-buster
MAINTAINER Brichet (brichet.b53@gmail.com)
ADD target/MS-Ressources-0.0.1-SNAPSHOT.jar /MS-Ressources.jar
CMD java -jar /MS-Ressources.jar
