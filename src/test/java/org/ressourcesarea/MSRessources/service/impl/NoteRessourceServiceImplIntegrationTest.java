package org.ressourcesarea.MSRessources.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSRessources.repository.NoteRessourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest
@Transactional
public class NoteRessourceServiceImplIntegrationTest {

    @Autowired
    private NoteRessourceServiceImpl classUnderTest;

    @Autowired
    private NoteRessourceRepository noteRessourceRepository;

    @BeforeEach
    void init(){

    }

    @Test
    void createNoteRessource(){

    }

    @Test
    void getNoteRessourceById(){

    }

    @Test
    void updateNoteRessource(){

    }

    @Test
    void deleteNoteRessource(){

    }
}
