package org.ressourcesarea.MSRessources.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSRessources.repository.RessourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
public class RessourceServiceImplIntegrationTest {

    @Autowired
    private RessourceServiceImpl classUnderTest;

    @Autowired
    private RessourceRepository ressourceRepository;

    @BeforeEach
    void init(){

    }

    @Test
    void createRessource (){

    }

    @Test
    void getRessourceByModule(){

    }

    @Test
    void getRessourceById(){

    }

    @Test
    void updateRessource(){

    }

    @Test
    void deleteRessources(){

    }
}
