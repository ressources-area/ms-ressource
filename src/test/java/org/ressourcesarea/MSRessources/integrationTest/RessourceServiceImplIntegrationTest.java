package org.ressourcesarea.MSRessources.integrationTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSRessources.application.error.EntityException;
import org.ressourcesarea.MSRessources.model.dtos.RessourceDto;
import org.ressourcesarea.MSRessources.model.entities.Ressource;
import org.ressourcesarea.MSRessources.repository.NoteRessourceRepository;
import org.ressourcesarea.MSRessources.repository.RessourceRepository;
import org.ressourcesarea.MSRessources.service.contract.RessourceService;
import org.ressourcesarea.MSRessources.service.contract.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
@Transactional
public class RessourceServiceImplIntegrationTest {

    @Autowired
    private RessourceService ressourceService;

    @Autowired
    private RessourceRepository ressourceRepository;

    @Autowired
    private NoteRessourceRepository noteRessourceRepository;

    @BeforeEach
    public void setUp(){
        Ressource ressource = new Ressource();
        ressource.setId(1);
        ressource.setModule("SUAP");
        ressource.setTitre("ressource1");
        ressource.setLien("totoLien");
        ressource.setDateCreation(LocalDate.now());

        ressourceRepository.save(ressource);

    }

    @Test
    void getRessourceByModule(){
        String module = "SUAP";

        List<RessourceDto> ressourceDtoList = ressourceService.getRessourcesByModule(module);
        assertThat(ressourceDtoList).hasSize(1);
    }


    @Test
    void deleteRessource_whenIdRessourceNotExist_shouldReturnEntityException() {
        EntityException exception =
            assertThrows(EntityException.class, () -> ressourceService.deleteRessources(5));
        assertThat(exception.getMessage()).isEqualTo("Aucune ressource à supprimer");
    }

    @Test
    void deleteRessource(){
        String module = "SUAP";

        StorageService storageService = mock(StorageService.class);

        doThrow(EntityException.class).when(storageService).deleteFile(anyString());

        ressourceService.deleteRessources(1);

        assertThat(ressourceService.getRessourcesByModule(module)).hasSize(0);
    }

}
