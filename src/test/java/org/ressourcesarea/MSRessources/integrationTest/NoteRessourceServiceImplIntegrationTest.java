package org.ressourcesarea.MSRessources.integrationTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSRessources.application.error.EntityException;
import org.ressourcesarea.MSRessources.model.dtos.NoteRessourceDto;
import org.ressourcesarea.MSRessources.model.entities.NoteRessource;
import org.ressourcesarea.MSRessources.model.entities.Ressource;
import org.ressourcesarea.MSRessources.repository.NoteRessourceRepository;
import org.ressourcesarea.MSRessources.repository.RessourceRepository;
import org.ressourcesarea.MSRessources.service.contract.NoteRessourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Transactional
public class NoteRessourceServiceImplIntegrationTest {

    @Autowired
    private NoteRessourceService noteRessourceService;

    @Autowired
    private NoteRessourceRepository noteRessourceRepository;

    @Autowired
    private RessourceRepository ressourceRepository;


    @BeforeEach
    public void setUp(){
        Ressource ressource = new Ressource();
        ressource.setDateCreation(LocalDate.now());
        ressource.setLien("totoLien");
        ressource.setModule("SUAP");
        ressource.setTitre("Titre");

        ressourceRepository.save(ressource);

        NoteRessource noteRessource = new NoteRessource();
        noteRessource.setNote("note de test");
        noteRessource.setTitre("titre de note");
        noteRessource.setRessource(ressourceRepository.getRessourceByTitre(ressource.getTitre()));

        noteRessourceRepository.save(noteRessource);

    }

    @Test
    void createNoteRessource(){
        // Récupération de l'idRessource save dans le setUp
        List<Ressource> ressourceList = ressourceRepository.getRessourceByModule("SUAP");
        Integer idRessource = ressourceList.get(0).getId();

        NoteRessourceDto noteRessourceDto = new NoteRessourceDto();
        noteRessourceDto.setNote("Test de la nouvelle note");
        noteRessourceDto.setTitre("Titre de la nouvelle note");

        noteRessourceService.createNoteRessource(noteRessourceDto, idRessource);
        List<NoteRessource> noteRessourceList = noteRessourceRepository.getListNoteRessourceByIdRessource(idRessource);
        assertThat(noteRessourceList).hasSize(2);

    }

    @Test
    void createNoteRessource_whenIdRessourceNotExiste_shouldReturnException(){
        NoteRessourceDto noteRessourceDto = new NoteRessourceDto();
        noteRessourceDto.setNote("TestBad");
        noteRessourceDto.setTitre("TitreBadTest");

        EntityException exception =
                assertThrows(EntityException.class, () ->
                        noteRessourceService.createNoteRessource(noteRessourceDto, 2));
        assertThat(exception.getMessage()).isEqualTo("Erreur lors de la création d'une note ressource");

    }

    @Test
    void getNoteRessourceById(){
        assertThat(noteRessourceService.getNoteRessourceById(1)).isNotNull();
    }

    @Test
    void getNoteRessourceById_whenIdNoteRessource_shouldReturnException() {
                assertThat(noteRessourceService.getNoteRessourceById(2)).isNull();
    }

    @Test
    void deleteNoteRessource(){
        noteRessourceService.deleteNoteRessource(1);
        assertThat(noteRessourceRepository.getListNoteRessourceByIdRessource(1)).hasSize(0);
    }

    @Test
    void deleteNoteRessource_whenIdNoteRessourceNotExist_shouldReturnEntityException(){
        assertThrows(EmptyResultDataAccessException.class,
            () -> noteRessourceService.deleteNoteRessource(100));
    }

}
