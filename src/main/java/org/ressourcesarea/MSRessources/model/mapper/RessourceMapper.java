package org.ressourcesarea.MSRessources.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.ressourcesarea.MSRessources.model.dtos.RessourceDto;
import org.ressourcesarea.MSRessources.model.entities.Ressource;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RessourceMapper {

    List<RessourceDto> mapToDto(List<Ressource> ressourceList);
    List<Ressource> map (List<RessourceDto> ressourceDtoList);
    RessourceDto fromEntityToDto(Ressource ressource);
    Ressource fromDtoToEntity(RessourceDto ressourceDto);
}
