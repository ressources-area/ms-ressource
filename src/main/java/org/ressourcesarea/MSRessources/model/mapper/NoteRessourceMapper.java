package org.ressourcesarea.MSRessources.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.ressourcesarea.MSRessources.model.dtos.NoteRessourceDto;
import org.ressourcesarea.MSRessources.model.dtos.RessourceDto;
import org.ressourcesarea.MSRessources.model.entities.NoteRessource;
import org.ressourcesarea.MSRessources.model.entities.Ressource;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NoteRessourceMapper {

    List<NoteRessourceDto> mapToDto(List<NoteRessource> noteRessourceList);
    List<NoteRessource> map(List<NoteRessourceDto> noteRessourceDtos);
    NoteRessourceDto fromEntityToDto(NoteRessource noteRessource);
    NoteRessource fromDtoToEntity(NoteRessourceDto noteRessourceDto);
}
