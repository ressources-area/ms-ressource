package org.ressourcesarea.MSRessources.model.dtos;

import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class RessourceDto {

    private Integer id;
    private String titre;
    private String lien;
    private LocalDate dateCreation;
    private String module;
    private List<NoteRessourceDto> noteRessourceList = new ArrayList<>();
}
