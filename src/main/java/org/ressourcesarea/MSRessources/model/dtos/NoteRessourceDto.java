package org.ressourcesarea.MSRessources.model.dtos;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class NoteRessourceDto {

    private Integer id;
    private String titre;
    private String note;

}
