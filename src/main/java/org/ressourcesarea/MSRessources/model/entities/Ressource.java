package org.ressourcesarea.MSRessources.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ressource")
@Data
public class Ressource implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "titre")
    private String titre;

    @Column(name = "lien")
    private String lien;

    @Column(name = "module")
    private String module;

    @Column(name = "datecreation")
    private LocalDate dateCreation;

    @OneToMany(targetEntity = NoteRessource.class,mappedBy = "ressource", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<NoteRessource> noteRessourceList = new ArrayList<>();

}
