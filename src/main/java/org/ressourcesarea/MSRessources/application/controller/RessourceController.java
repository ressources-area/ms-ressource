package org.ressourcesarea.MSRessources.application.controller;

import org.ressourcesarea.MSRessources.application.error.GeneralException;
import org.ressourcesarea.MSRessources.model.dtos.RessourceDto;
import org.ressourcesarea.MSRessources.service.contract.NoteRessourceService;
import org.ressourcesarea.MSRessources.service.contract.RessourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin(value = {"http://localhost:4200"}, methods = {GET,POST,PUT,DELETE,OPTIONS})
@RestController
@RequestMapping("ressource")
public class RessourceController {

    // ----- Injections des dépedances ----- //

    private final RessourceService ressourceService;
    private final NoteRessourceService noteRessourceService;

    @Autowired
    public RessourceController(RessourceService ressourceService,
                               NoteRessourceService noteRessourceService){
        this.ressourceService = ressourceService;
        this.noteRessourceService = noteRessourceService;
    }

    // ----- Méthodes ----- //

    /**
     * Récupère la liste des ressources selon le module
     * @param module
     * @return
     */
    @GetMapping("/ressourceByModule/{module}")
    public List<RessourceDto> getRessouceDtoByModule(@PathVariable("module") String module){
        return ressourceService.getRessourcesByModule(module);
    }

    /**
     * Récupère la ressource selon son id
     * @param idRessource
     * @return
     */
    @GetMapping("/ressourceInfo/{idRessource}")
    public RessourceDto getRessourceById(@PathVariable("idRessource") Integer idRessource){
        return ressourceService.getRessourceById(idRessource);
    }

    /**
     * Modification de la ressource
     * @param ressourceDto
     */
    @PutMapping("/update")
    public RessourceDto updateRessource(@RequestBody RessourceDto ressourceDto){
        try {
            ressourceService.updateRessource(ressourceDto);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la modification de la ressource", e
            );
        }
        return ressourceDto;
    }

    /**
     * Suppression d'une ressource
     * @param idRessource
     */
    @DeleteMapping("/delete/{idRessource}")
    public void deleteRessource(@PathVariable("idRessource") Integer idRessource){
        try {
            ressourceService.deleteRessources(idRessource);
        } catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la suppression de la ressource"
            );
        }
    }
}
