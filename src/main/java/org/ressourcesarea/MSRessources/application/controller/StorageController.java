package org.ressourcesarea.MSRessources.application.controller;

import org.ressourcesarea.MSRessources.application.error.GeneralException;
import org.ressourcesarea.MSRessources.service.contract.RessourceService;
import org.ressourcesarea.MSRessources.service.contract.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping("/file")
public class StorageController {

    // ----- Injections des dépendances ----- //

    private final StorageService storageService;
    private final RessourceService ressourceService;


    @Autowired
    public StorageController(StorageService storageService,
                             RessourceService ressourceService){
        this.storageService = storageService;
        this.ressourceService = ressourceService;
    }

    // ----- Méthodes ----- //

    /**
     * Stocke un document dans le S3
     * @param file
     * @return
     */
    @PostMapping(value = "/upload/{module}")
    public ResponseEntity<String> uploadFile(@RequestParam("file")MultipartFile file,
                                             @PathVariable("module") String module){
        try {
            ressourceService.createRessource(module, file);
            return new ResponseEntity<>("Uploaded", HttpStatus.OK);
        } catch (GeneralException e) {
                        throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de l'ajout d'un document"
            );
        }

    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity<ByteArrayResource> downloadFIle(@PathVariable String fileName){
        byte[] data = storageService.downloadFile(fileName);
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity
                .ok()
                .contentLength(data.length)
                .header("content-type","application/octet-stream")
                .header("content-disposition","attachment; filename=\""+ fileName +"\"")
                .body(resource);
    }

    /**
     * Supprime le document
     * @param fileName
     * @return
     */
    @DeleteMapping("/delete/{fileName}")
    public ResponseEntity<String> deleteFile(@PathVariable String fileName){
        try {
            return new ResponseEntity<>(storageService.deleteFile(fileName), HttpStatus.OK);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la suppression d'un fichier"
            );
        }
    }


}
