package org.ressourcesarea.MSRessources.application.controller;

import lombok.extern.flogger.Flogger;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.ressourcesarea.MSRessources.application.error.GeneralException;
import org.ressourcesarea.MSRessources.model.dtos.NoteRessourceDto;
import org.ressourcesarea.MSRessources.service.contract.NoteRessourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Log4j2
@CrossOrigin(value = {"http://localhost:4200"}, methods = {GET,POST,PUT,DELETE,OPTIONS})
@RestController
@RequestMapping("noteRessource")
public class NoteRessourceController {

    // ----- Injections des dépendances ----- //

    private final NoteRessourceService noteRessourceService;

    @Autowired
    public NoteRessourceController(NoteRessourceService noteRessourceService) {
        this.noteRessourceService = noteRessourceService;
    }

    // ----- Méthodes ----- //

    /**
     * Création d'un note ressource
     *
     * @param noteRessourceDto
     */
    @PostMapping(value ="/create/{idRessource}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public NoteRessourceDto createNoteRessource(@RequestBody NoteRessourceDto noteRessourceDto,
                                                @PathVariable("idRessource") Integer idRessource) {
        try {
            return noteRessourceService.createNoteRessource(noteRessourceDto, idRessource);
        }
        catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la création de la note ressource"
            );
        }
    }

    /**
     * Récupère la note ressource selon son id
     * @param idNoteRessource
     * @return
     */
    @GetMapping("/{idNoteRessource}")
    public NoteRessourceDto getNoteRessourceById(@PathVariable("idNoteRessource")Integer idNoteRessource){
        return noteRessourceService.getNoteRessourceById(idNoteRessource);
    }

    /**
     * Récupère la liste des notes d'une ressource
     * @param idRessource
     * @return
     */
    @GetMapping("/ressource/{idRessource}")
    public List<NoteRessourceDto> getListNoteRessourceByIdRessource(@PathVariable("idRessource") Integer idRessource){
        return noteRessourceService.getListNoteRessourceByIdRessource(idRessource);
    }

    /**
     * Modification d'une note ressource
     * @param ressourceDto
     */
    @PutMapping("/update")
    public void updateNoteRessource(@RequestBody NoteRessourceDto ressourceDto) {
        try {
            noteRessourceService.updateNoteRessource(ressourceDto);
        }
        catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la modification de la note ressource"
            );
        }
    }

    /**
     * Supprime la note de la ressource
     * @param idNoteRessource
     */
    @DeleteMapping("/delete/{idNoteRessource}")
    public void deleteNoteRessource(@PathVariable("idNoteRessource") Integer idNoteRessource){
        try {
            noteRessourceService.deleteNoteRessource(idNoteRessource);
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la suppression d'une note ressource"
            );
        }
    }
}
