package org.ressourcesarea.MSRessources.application.error;

public class AuthorizeException extends RuntimeException {
    public AuthorizeException(String message){
        super(message);
    }
}
