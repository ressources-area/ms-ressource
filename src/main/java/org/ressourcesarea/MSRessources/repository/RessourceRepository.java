package org.ressourcesarea.MSRessources.repository;

import org.ressourcesarea.MSRessources.model.entities.Ressource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RessourceRepository extends JpaRepository<Ressource, Integer> {

    /**
     * Récupère la liste des ressources selon le module
     * @param module
     * @return
     */
    @Query("SELECT r FROM Ressource r WHERE r.module = :module")
    List<Ressource> getRessourceByModule(@Param("module") String module);

    /**
     * Récupère la ressource selon son id
     * @param idRessource
     * @return
     */
    @Query("SELECT r FROM Ressource r WHERE r.id = :idRessource")
    Ressource getRessourceById(@Param("idRessource") Integer idRessource);

    /**
     * Récupère le document selon le titre
     * @param titre
     * @return
     */
    @Query("SELECT r FROM Ressource r WHERE r.titre = :titre")
    Ressource getRessourceByTitre(@Param("titre") String titre);

    @Transactional
    @Modifying
    @Query("UPDATE Ressource r SET r = :ressource WHERE r = :ressource")
    void updateRessource(@Param("ressource")Ressource ressource);
}
