package org.ressourcesarea.MSRessources.repository;

import org.ressourcesarea.MSRessources.model.entities.NoteRessource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRessourceRepository extends JpaRepository<NoteRessource, Integer> {

    /**
     * Récupère la ressource selon son id
     * @param idNoteRessource
     * @return
     */
    @Query("SELECT n FROM NoteRessource n WHERE n.id = :idNoteRessource")
    NoteRessource getNoteRessourceById(@Param("idNoteRessource") Integer idNoteRessource);

    /**
     * Récupère la liste des notes d'une ressource
     * @param idRessource
     * @return
     */
    @Query("SELECT n FROM NoteRessource n WHERE n.ressource.id = :idRessource")
    List<NoteRessource> getListNoteRessourceByIdRessource(@Param("idRessource") Integer idRessource);
}
