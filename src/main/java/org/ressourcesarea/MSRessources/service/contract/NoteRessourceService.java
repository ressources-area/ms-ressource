package org.ressourcesarea.MSRessources.service.contract;

import org.ressourcesarea.MSRessources.model.dtos.NoteRessourceDto;
import org.ressourcesarea.MSRessources.model.dtos.RessourceDto;

import java.util.List;

public interface NoteRessourceService {

    /**
     * Création NoteRessources
     */
    NoteRessourceDto createNoteRessource(NoteRessourceDto ressourceDto, Integer idRessource);

    /**
     * Récupère la note de la ressource selon son id
     * @param idNoteRessource
     * @return List
     */
    NoteRessourceDto getNoteRessourceById(Integer idNoteRessource);

    /**
     * Récuèpre la liste des note d'une ressource
     * @param idRessource
     * @return
     */
    List<NoteRessourceDto> getListNoteRessourceByIdRessource(Integer idRessource);

    /**
     * Modifier NoteRessources
     */
    NoteRessourceDto updateNoteRessource(NoteRessourceDto noteRessourceDto);

    /**
     * Supprime la note ressouce
     * @param idNoteRessource
     */
    void deleteNoteRessource(Integer idNoteRessource);
}
