package org.ressourcesarea.MSRessources.service.contract;

import org.springframework.web.multipart.MultipartFile;

import java.net.URL;

public interface StorageService {

    String uploadFile(MultipartFile file);

    byte[] downloadFile(String fileName);

    String deleteFile(String fileName);

    URL getUrl(String fileName);
}
