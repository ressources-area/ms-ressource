package org.ressourcesarea.MSRessources.service.contract;

import org.ressourcesarea.MSRessources.model.dtos.RessourceDto;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.util.List;

public interface RessourceService {

    /**
     * Create Ressources
     * @param module
     */
    void createRessource(String module, MultipartFile file);

    /**
     * Get ressources by module
     * @param module
     * @return
     */
    List<RessourceDto> getRessourcesByModule(String module);

    /**
     * Récupère la ressource selon son id
     * @param idRessource
     * @return
     */
    RessourceDto getRessourceById(Integer idRessource);

    /**
     * update ressources
     * @param ressourceDto
     */
    void updateRessource(RessourceDto ressourceDto);

    /**
     * Delete ressources
     * @param idRessources
     */
    void deleteRessources(Integer idRessources);
}
