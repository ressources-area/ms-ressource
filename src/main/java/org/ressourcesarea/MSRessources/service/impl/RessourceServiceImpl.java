package org.ressourcesarea.MSRessources.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.ressourcesarea.MSRessources.application.error.EntityException;
import org.ressourcesarea.MSRessources.model.dtos.RessourceDto;
import org.ressourcesarea.MSRessources.model.entities.Ressource;
import org.ressourcesarea.MSRessources.model.mapper.RessourceMapper;
import org.ressourcesarea.MSRessources.repository.RessourceRepository;
import org.ressourcesarea.MSRessources.service.contract.RessourceService;
import org.ressourcesarea.MSRessources.service.contract.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
public class RessourceServiceImpl implements RessourceService {

    // ----- Injections des dépendances ----- //

    private final RessourceRepository ressourceRepository;
    private final RessourceMapper ressourceMapper;
    private final StorageService storageService;

    @Autowired
    public RessourceServiceImpl(RessourceRepository ressourceRepository,
                                RessourceMapper ressourceMapper,
                                StorageService storageService){
        this.ressourceRepository = ressourceRepository;
        this.ressourceMapper = ressourceMapper;
        this.storageService = storageService;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param module
     */
    @Override
    @Transactional
    public void createRessource(String module, MultipartFile file) {
        try {
            String titre = file.getOriginalFilename();
            if ( ressourceRepository.getRessourceByTitre(titre) == null){
                storageService.uploadFile(file);
                URL url = storageService.getUrl(titre);
                String lienUrl = url.getProtocol()+"://"+url.getHost()+url.getPath();
                RessourceDto ressourceDto = new RessourceDto();
                ressourceDto.setDateCreation(LocalDate.now());
                ressourceDto.setLien(lienUrl);
                ressourceDto.setTitre(titre);
                ressourceDto.setModule(module);
                ressourceRepository.save(ressourceMapper.fromDtoToEntity(
                        ressourceDto
                ));
            } else {
                log.error("La ressource existe déjà");
                throw new EntityException("Une ressource du même titre existe déjà");
            }
        } catch (EntityException e) {
            log.error("Erreur de création d'une ressource");
            throw new EntityException("Erreur lors de l'ajout d'un document");
        }
    }

    /**
     * {@inheritDoc}
     * @param module
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<RessourceDto> getRessourcesByModule(String module) {
        return ressourceMapper.mapToDto(ressourceRepository.getRessourceByModule(module));
    }

    /**
     * {@inheritDoc}
     * @param idRessource
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public RessourceDto getRessourceById(final Integer idRessource) {
            return ressourceMapper.fromEntityToDto(ressourceRepository.getRessourceById(idRessource));
    }

    /**
     * {@inheritDoc}
     * @param ressourceDto
     */
    @Override
    @Transactional
    public void updateRessource(RessourceDto ressourceDto) {
        try {
            ressourceMapper.fromEntityToDto(ressourceRepository.save(
                    ressourceMapper.fromDtoToEntity(ressourceDto)
            ));
        }catch (EntityException e){
            log.error("Erreur lors de la modification d'une ressource");
            throw new EntityException("Erreur lors de la modification d'une ressource");
        }
    }

    /**
     * {@inheritDoc}
     * @param idRessources
     */
    @Override
    @Transactional
    public void deleteRessources(final Integer idRessources) {
            Ressource ressource = ressourceRepository.getRessourceById(idRessources);
            if(ressource != null){
                ressourceRepository.delete(ressourceRepository.getRessourceById(idRessources));
                storageService.deleteFile(ressource.getTitre());
            } else {
                log.error("Erreur lors de la suppression d'une ressource");
                throw new EntityException("Aucune ressource à supprimer");
            }
    }
}
