package org.ressourcesarea.MSRessources.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.ressourcesarea.MSRessources.application.error.GeneralException;
import org.ressourcesarea.MSRessources.service.contract.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

@Slf4j
@Service
public class StorageServiceImpl implements StorageService {

    @Value("${application.bucket.name}")
    private String bucketName;

    @Autowired
    private AmazonS3 s3Client;

    /**
     * Upload un document
     * @param file
     * @return
     */
    public String uploadFile(MultipartFile file) {
        File fileObject = convertMultiPartFileToFile(file);
        String fileName = file.getOriginalFilename();
        s3Client.putObject(new PutObjectRequest(bucketName,fileName,fileObject));
        fileObject.delete();

        return fileName;
    }

    /**
     * Download un document
     * @param fileName
     * @return
     */
    public byte[] downloadFile(String fileName){
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte [] content = IOUtils.toByteArray(inputStream);
            return content;
        } catch (IOException e) {
            log.error("Erreur lors du download du fichier");
            e.printStackTrace();
        }
        return null;
    }

    public String deleteFile(String fileName) {
        if (s3Client.getUrl(bucketName,fileName) != null){
            s3Client.deleteObject(bucketName,fileName);
            return fileName+" removed...";
        } else {
            log.error("Erreur de suppression d'une ressource");
            throw new GeneralException("Aucun document de ce nom existe et peu être supprimé");
        }
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)){
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Erreur durant la convertion d'un MultipartFile en File");
        }
        return convertedFile;
    }

    @Override
    public URL getUrl(final String fileName) {
        return s3Client.getUrl(bucketName,fileName);
    }

}
