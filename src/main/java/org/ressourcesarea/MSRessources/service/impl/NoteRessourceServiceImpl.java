package org.ressourcesarea.MSRessources.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.ressourcesarea.MSRessources.application.error.EntityException;
import org.ressourcesarea.MSRessources.model.dtos.NoteRessourceDto;
import org.ressourcesarea.MSRessources.model.entities.NoteRessource;
import org.ressourcesarea.MSRessources.model.entities.Ressource;
import org.ressourcesarea.MSRessources.model.mapper.NoteRessourceMapper;
import org.ressourcesarea.MSRessources.repository.NoteRessourceRepository;
import org.ressourcesarea.MSRessources.repository.RessourceRepository;
import org.ressourcesarea.MSRessources.service.contract.NoteRessourceService;
import org.ressourcesarea.MSRessources.service.contract.RessourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class NoteRessourceServiceImpl implements NoteRessourceService {

    // ----- Injections des dépendances ----- //

    private final NoteRessourceRepository noteRessourceRepository;
    private final NoteRessourceMapper noteRessourceMapper;
    private final RessourceService ressourceService;
    private final RessourceRepository ressourceRepository;

    @Autowired
    public NoteRessourceServiceImpl(NoteRessourceRepository noteRessourceRepository,
                                    NoteRessourceMapper noteRessourceMapper,
                                    RessourceService ressourceService,
                                    RessourceRepository ressourceRepository){
        this.noteRessourceRepository = noteRessourceRepository;
        this.noteRessourceMapper = noteRessourceMapper;
        this.ressourceService = ressourceService;
        this.ressourceRepository = ressourceRepository;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param noteRessourceDto
     */
    @Override
    @Transactional
    public NoteRessourceDto createNoteRessource(final NoteRessourceDto noteRessourceDto, Integer idRessource) {
            Ressource ressource = ressourceRepository.getRessourceById(idRessource);
            if(ressource != null){
                NoteRessource noteRessource = noteRessourceMapper.fromDtoToEntity(noteRessourceDto);
                noteRessource.setRessource(ressource);
                return noteRessourceMapper.fromEntityToDto(noteRessourceRepository.save(noteRessource));
            } else {
                log.error("Erreur de création d'une note ressource");
                throw new EntityException("Erreur lors de la création d'une note ressource");
            }
    }

    /**
     * {@inheritDoc}
     * @param idNoteRessource
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public NoteRessourceDto getNoteRessourceById(final Integer idNoteRessource) {
        NoteRessourceDto noteRessourceDto = noteRessourceMapper.fromEntityToDto(
                noteRessourceRepository.getNoteRessourceById(idNoteRessource));
        return noteRessourceDto;
    }

    /**
     * {@inheritDoc}
     * @param idRessource
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<NoteRessourceDto> getListNoteRessourceByIdRessource(final Integer idRessource) {
        return noteRessourceMapper.mapToDto(noteRessourceRepository.getListNoteRessourceByIdRessource(idRessource));
    }

    /**
     * {@inheritDoc}
     * @param noteRessourceDto
     */
    @Override
    @Transactional
    public NoteRessourceDto updateNoteRessource(final NoteRessourceDto noteRessourceDto) {
        return noteRessourceMapper.fromEntityToDto(noteRessourceRepository.save(noteRessourceMapper.fromDtoToEntity(
                noteRessourceDto)
        ));
    }

    /**
     * {@inheritDoc}
     * @param idNoteRessource
     */
    @Override
    @Transactional
    public void deleteNoteRessource(final Integer idNoteRessource) {
        noteRessourceRepository.deleteById(idNoteRessource);
    }
}
