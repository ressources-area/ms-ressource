package org.ressourcesarea.MSRessources;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("org.ressourcesarea.MSRessources")
public class MsRessourcesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsRessourcesApplication.class, args);
		System.out.println("MS-Ressources start");
	}

}
